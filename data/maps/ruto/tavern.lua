local map = ...
local game = map:get_game()

function walking_npc_waitress:on_interaction()
  local mine_opening_quest = game:get_value("mine_opening_quest")

  if mine_opening_quest == nil or mine_opening_quest < 3 then
    game:start_dialog("ruto.tavern.waitress.was_crowded")
  else
    game:start_dialog("ruto.tavern.waitress.missing_miner")
    if mine_opening_quest == 3 then
      game:set_value("mine_opening_quest", 4)
    end
  end
end